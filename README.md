# AutoCorrector

With this project, an application has been developed that offers a high-performance text suggestion against typos.

## Setup

### How to run the application

1. Clone the repository
2. Open the project in Intellij IDEA
3. Edit the main method in the class `Main.java`
4. Run the main method in the class `Main.java`

## Examples

### Example 1

```java
String wrongText = "Hellı, mu name is John";
corrector.correctText(wrongText); // Hello, my name is John
```

### Example 2

```java
String wrongText = "I abm a studebt at the Unıversıty of Münster";
corrector.correctText(wrongText); // I am a student at the University of Monster
```