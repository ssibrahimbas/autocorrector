package com.java.lib.dictionary;

// this class is used to load dictionary into the corrector
public interface IDictionaryCallback {
    void add(String word);
}
